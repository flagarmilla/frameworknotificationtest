//
//  AppDelegate.h
//  gapsdktests
//
//  Created by Federico Lagarmilla on 27/7/15.
//  Copyright (c) 2015 gap. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

