//
//  framewotktest.h
//  framewotktest
//
//  Created by Federico Lagarmilla on 27/7/15.
//  Copyright (c) 2015 flagarmilla. All rights reserved.

// **** INCLUDES Nested Framework new *****

#import <UIKit/UIKit.h>

//! Project version number for framewotktest.
FOUNDATION_EXPORT double framewotktestVersionNumber;

//! Project version string for framewotktest.
FOUNDATION_EXPORT const unsigned char framewotktestVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <framewotktest/PublicHeader.h>

#import <framewotktest/TestInstance.h>