//
//  TestInstance.h
//  framewotktest
//
//  Created by Federico Lagarmilla on 27/7/15.
//  Copyright (c) 2015 flagarmilla. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *mockNotification = @"MockNotification";

@protocol TestInstanceDelegate <NSObject>

- (void)mockNotificationReceived;

@end

@interface TestInstance : NSObject

+ (TestInstance *)instance;

@property (nonatomic, weak) id<TestInstanceDelegate> delegate;

@end