//
//  AppDelegate.h
//  GapMockEvents
//
//  Created by Federico Lagarmilla on 29/7/15.
//  Copyright (c) 2015 flagarmilla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

