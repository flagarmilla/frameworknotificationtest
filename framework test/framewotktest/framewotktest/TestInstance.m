//
//  TestInstance.m
//  framewotktest
//
//  Created by Federico Lagarmilla on 27/7/15.
//  Copyright (c) 2015 flagarmilla. All rights reserved.
//

#import "TestInstance.h"
//#import <NestedFrameworkTest/NestedUtils.h>

@interface TestInstance ()


@end

@implementation TestInstance

+ (TestInstance *)instance
{
    static TestInstance *singleton = nil;
    
    if (nil == singleton) {
        singleton = [[TestInstance alloc] init];
    }
    return singleton;
}

- (id)init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mockNotificationHandler:) name:mockNotification object:nil];
    }
    return self;
}

- (void)mockNotificationHandler:(NSNotification *)notification
{
//    NSString *testNest = [NestedUtils nestedString];
//    NSLog(@"%@", testNest);
    if ([self.delegate respondsToSelector:@selector(mockNotificationReceived)]) {
        [self.delegate mockNotificationReceived];        
    }
}

@end
