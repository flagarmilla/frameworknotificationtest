//
//  main.m
//  gapsdktests
//
//  Created by Federico Lagarmilla on 27/7/15.
//  Copyright (c) 2015 gap. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
