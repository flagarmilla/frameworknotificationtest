//
//  ViewController.m
//  gapsdktests
//
//  Created by Federico Lagarmilla on 27/7/15.
//  Copyright (c) 2015 gap. All rights reserved.
//

#import "ViewController.h"
#import <framewotktest/TestInstance.h>

@interface ViewController () <TestInstanceDelegate>

@property (nonatomic, weak) IBOutlet UILabel *lblStatus;
@property (nonatomic, weak) IBOutlet UILabel *lblTimeAgo;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[TestInstance instance] setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNotificationTouch:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:mockNotification object:nil];

}

- (void)mockNotificationReceived
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *datestr = [dateFormatter stringFromDate:[NSDate date]];
    NSLog(@"scanned event received at: %@", datestr);
    [self.lblTimeAgo setText:[NSString stringWithFormat:@"Scan Event At: %@", datestr]];
}




@end
